package com.comparus.test_task.JavaTest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * DataSourceConfiguration
 */
@Configuration
public class DataSourceConfiguration {

	@Bean
	public DataSourceGeneratePostProcessor dataSourceGeneratePostProcessor(Environment environment){
		return new DataSourceGeneratePostProcessor(environment);
	}
	
}
