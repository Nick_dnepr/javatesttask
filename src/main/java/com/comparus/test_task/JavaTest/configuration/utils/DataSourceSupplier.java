package com.comparus.test_task.JavaTest.configuration.utils;

import java.util.function.Supplier;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;

import com.comparus.test_task.JavaTest.configuration.models.DataSourceConfigItem;

/**
 * DataSourceSupplier
 */
public class DataSourceSupplier implements Supplier<DataSource> {

	private DataSourceConfigItem configItem;

	public DataSourceSupplier(DataSourceConfigItem configItem) {
		this.configItem = configItem;
	}

	@Override
	public DataSource get() {
		DataSourceProperties properties = new DataSourceProperties();
		properties.setName(configItem.getName());
		properties.setUrl(configItem.getUrl());
		properties.setUsername(configItem.getUser());
		properties.setPassword(configItem.getPassword());
		properties.determineDriverClassName();

		return properties.initializeDataSourceBuilder().build();
	}

}
