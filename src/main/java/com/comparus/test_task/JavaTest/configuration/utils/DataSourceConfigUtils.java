package com.comparus.test_task.JavaTest.configuration.utils;

import java.util.List;
import java.util.stream.Collectors;

import com.comparus.test_task.JavaTest.configuration.models.DataSourceConfigItem;

/**
 * DataSourceConfigUtils
 */
public class DataSourceConfigUtils {

	// TODO make up some real validation
	public static boolean validate(DataSourceConfigItem item) {
		if (item.getUrl() == null) {
			return false;
		}
		if (item.getTable() == null) {
			return false;
		}

		return true;
	}

	public static String generateUniqueName(List<DataSourceConfigItem> list) {
		List<String> names = list.stream().map(item -> item.getName()).filter(name -> name != null)
				.collect(Collectors.toList());

		String base = "datasource_";
		int number = 0;
		String name = base + number;

		while (names.contains(name)) {
			number++;
			name = base + number;
		}
		return name;
	}

}
