package com.comparus.test_task.JavaTest.configuration;

import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import com.comparus.test_task.JavaTest.configuration.models.DataSourceConfigItem;
import com.comparus.test_task.JavaTest.configuration.utils.DataSourceConfigUtils;
import com.comparus.test_task.JavaTest.configuration.utils.DataSourceSupplier;
import com.comparus.test_task.JavaTest.data.UserRepositoryJdbc;

/**
 * DataSourceGeneratePostProcessor
 */
public class DataSourceGeneratePostProcessor implements BeanDefinitionRegistryPostProcessor {

	private static final String hard = "hard";
	private static final String soft = "soft";

	private List<DataSourceConfigItem> dataSources;
	private boolean dropOnValidate;

	private Logger logger = LoggerFactory.getLogger(DataSourceGeneratePostProcessor.class);

	public DataSourceGeneratePostProcessor(Environment environment) {

		dataSources = Binder.get(environment).bind("data-sources", Bindable.listOf(DataSourceConfigItem.class))
				.orElse(null);

		if (dataSources == null) {
			logger.error(
					"data-sources parameter has not been provided! Please add data-sources config and re-run the application, otherwise it will be useless!");
			dataSources = Collections.emptyList();
		}

		String validateMode = Binder.get(environment).bind("ds-validate-mode", Bindable.of(String.class)).orElse(null);
		if (validateMode == null) {
			logger.info("No ds-validate-mode has been provided. Setting mode to 'hard'");
			validateMode = hard;
		}
		if (!validateMode.equalsIgnoreCase(hard) && !validateMode.equalsIgnoreCase(soft)) {
			logger.error("Invalid ds-validate-mode has been provided! Available options: 'soft', 'hard'");
			throw new IllegalArgumentException("ds-validate-mode is invalid! Please change your configuration.");
		}
		dropOnValidate = validateMode.equalsIgnoreCase(hard);

		System.out.println();
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		for (DataSourceConfigItem item : dataSources) {
			if (!DataSourceConfigUtils.validate(item)) {
				logger.warn("Failed to validate item {}", item.toString());
				if (dropOnValidate) {
					throw new IllegalArgumentException(
							"Provided datasource configuration is invalid! Please fix your configuration or set ds-validate-mode to 'soft' to ignore invalid items.");
				} else {
					logger.warn("Skipping invalid config item!");
					continue;
				}
			}

			String dsBeanName = item.getName();
			if (dsBeanName == null) {
				dsBeanName = DataSourceConfigUtils.generateUniqueName(dataSources);
				item.setName(dsBeanName);
			} else if (registry.isBeanNameInUse(dsBeanName)) {
				item.setName(null);
				dsBeanName = DataSourceConfigUtils.generateUniqueName(dataSources);
				item.setName(dsBeanName);
			}

			BeanDefinition dataSourceDef = dataSourceBeanDefinition(item);
			registry.registerBeanDefinition(dsBeanName, dataSourceDef);

			BeanDefinition jdbcTemplateRef = jdbcTemplateDefinition(dsBeanName);
			String jdbcBeanName = dsBeanName + "_JDBC";

			registry.registerBeanDefinition(jdbcBeanName, jdbcTemplateRef);

			String jdbcUserRepositoryBeanName = jdbcBeanName + "_REPOSITORY";
			BeanDefinition jdbcUserRepositoryRef = jdbcUserRepositoryDefinition(jdbcBeanName, item);
			registry.registerBeanDefinition(jdbcUserRepositoryBeanName, jdbcUserRepositoryRef);
		}
	}

	private BeanDefinition dataSourceBeanDefinition(DataSourceConfigItem item) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(DataSource.class,
				new DataSourceSupplier(item));
		return builder.getBeanDefinition();
	}

	private BeanDefinition jdbcTemplateDefinition(String dsBeanName) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(JdbcTemplate.class);
		builder.addConstructorArgReference(dsBeanName);
		return builder.getBeanDefinition();
	}

	private BeanDefinition jdbcUserRepositoryDefinition(String jdbcTemplateRef, DataSourceConfigItem item) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(UserRepositoryJdbc.class);
		builder.addConstructorArgReference(jdbcTemplateRef);
		builder.addConstructorArgValue(item);
		return builder.getBeanDefinition();
	}

}
