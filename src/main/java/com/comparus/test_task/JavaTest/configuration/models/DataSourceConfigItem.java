package com.comparus.test_task.JavaTest.configuration.models;

import java.util.Collections;
import java.util.Map;

/**
 * DataSourceConfig
 */
public class DataSourceConfigItem {

	private String name;
	private String strategy;
	private String url;
	private String table;
	private String password;
	private Map<String, String> mappings;

	private String user;

	public DataSourceConfigItem() {
		this(null, null, null, null, null, null, null);
	}

	public DataSourceConfigItem(final String name, final String strategy, final String url, final String table,
			final String user, final String password,
			final Map<String, String> mappings) {
		this.name = name;
		this.strategy = strategy;
		this.url = url;
		this.table = table;
		this.user = user;
		this.password = password;
		this.mappings = mappings != null ? mappings : Collections.emptyMap();
	}

	public String getName() {
		return name;
	}

	public String getStrategy() {
		return strategy;
	}

	public String getUrl() {
		return url;
	}

	public String getTable() {
		return table;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public Map<String, String> getMappings() {
		return Map.copyOf(mappings);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMappings(Map<String, String> mappings) {
		this.mappings = mappings != null ? mappings : Collections.emptyMap();
	}

	@Override
	public String toString() {
		return "DataSourceConfigItem [name=" + name + ", url=" + url + ", table=" + table + ", user=" + user + "]";
	}

}
