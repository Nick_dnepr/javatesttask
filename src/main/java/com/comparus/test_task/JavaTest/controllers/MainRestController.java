package com.comparus.test_task.JavaTest.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.comparus.test_task.JavaTest.api.UsersApi;
import com.comparus.test_task.JavaTest.model.User;
import com.comparus.test_task.JavaTest.service.UserService;

/**
 * MainRestController
 */
@RestController
public class MainRestController implements UsersApi {

	private UserService userService;

	public MainRestController(UserService userService) {
		this.userService = userService;
	}

	@Override
	public ResponseEntity<List<User>> getAllUsers() {
		return ResponseEntity.ok(userService.getAll());
	}
}
