package com.comparus.test_task.JavaTest.service;

import java.util.List;

import com.comparus.test_task.JavaTest.model.User;

/**
 * UserService
 */
public interface UserService {

	List<User> getAll();
}
