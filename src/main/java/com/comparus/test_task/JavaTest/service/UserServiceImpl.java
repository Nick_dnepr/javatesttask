package com.comparus.test_task.JavaTest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.comparus.test_task.JavaTest.data.UserRepository;
import com.comparus.test_task.JavaTest.model.User;

/**
 * UserServiceImpl
 */
@Service
public class UserServiceImpl implements UserService {

	private List<UserRepository> repositories;


	public UserServiceImpl(List<UserRepository> repositories) {
		this.repositories = repositories;
	}

	@Override
	public List<User> getAll() {
		List<User> users = new ArrayList<>();
		for(UserRepository repository: repositories){
			users.addAll(repository.getAll());
		}
		return users;
	}

}
