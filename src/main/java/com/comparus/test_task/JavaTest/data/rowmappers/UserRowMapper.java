package com.comparus.test_task.JavaTest.data.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.comparus.test_task.JavaTest.model.User;

/**
 * UserRowMapper
 */
public class UserRowMapper implements RowMapper<User> {

	public static final String ID = "id";
	public static final String USERNAME = "username";
	public static final String NAME = "name";
	public static final String SURNAME = "surname";

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {

		User user = new User();

		user.setId(rs.getString(ID));
		user.setUsername(rs.getString(USERNAME));
		user.setName(rs.getString(NAME));
		user.setSurname(rs.getString(SURNAME));

		return user;
	}

}
