package com.comparus.test_task.JavaTest.data;

import java.util.List;

import com.comparus.test_task.JavaTest.model.User;


/**
 * UserRepository
 */
public interface UserRepository {

	List<User> getAll();		
}
