package com.comparus.test_task.JavaTest.data;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.comparus.test_task.JavaTest.configuration.models.DataSourceConfigItem;
import com.comparus.test_task.JavaTest.data.rowmappers.UserRowMapper;
import com.comparus.test_task.JavaTest.model.User;

/**
 * UserRepositoryJdbc
 */
public class UserRepositoryJdbc implements UserRepository {

	private JdbcTemplate template;
	private DataSourceConfigItem configItem;

	public UserRepositoryJdbc(JdbcTemplate template, DataSourceConfigItem configItem) {
		this.template = template;
		this.configItem = configItem;
	}

	@Override
	public List<User> getAll() {
		Map<String, String> mappings = configItem.getMappings();
		return template.query(
				String.format("select %s as id, %s as username, %s as name, %s as surname from %s",
						mappings.getOrDefault(UserRowMapper.ID, UserRowMapper.ID),
						mappings.getOrDefault(UserRowMapper.USERNAME, UserRowMapper.USERNAME),
						mappings.getOrDefault(UserRowMapper.NAME, UserRowMapper.NAME),
						mappings.getOrDefault(UserRowMapper.SURNAME, UserRowMapper.SURNAME),
						configItem.getTable()
				),
				new UserRowMapper());
	}

}
