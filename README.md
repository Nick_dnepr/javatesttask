# JAVA TEST ASSIGNMENT 
A simple spring boot API, created as a test for a vacancy application.

---

## Requirements:
1. JDK 17+
2. Maven

---

## Launch steps
1. `git clone https://gitlab.com/Nick_dnepr/javatesttask` to your desired location
2. `cd javatesttask`
3. `mvn clean package`
4. Copy executable .jar file from `target/JavaTest-0.0.1-SNAPSHOT.jar` to your desired location.
5. Create `application.yaml` file in the same directory where you copied .jar file. More on configuration in the next section
6. Launch jar with `java -jar JavaTest-0.0.1-SNAPSHOT.jar`

---

## Configuration
`ds-validate-mode` - mode of datasources validation. Can be either `hard` or `soft`. In hard mode application generates an exception and exits. 
In soft mode invalid datasources are skipped and application starts with valid ones. If option is not provided the application will start in hard mode.
`data-sources` - list of datasource configurations. Each datasource item has a list of parameters:
1. `name` - name of datasource. Will be automatically generated if not provided by config. Names should be unique. If application detects a duplicate name
it will generate a new one.
2. `url` - JDBC url to the database. Mandatory parameter. If not provided, item validation will fail.
3. `table` - name of the table from which application will select data. Mandatory parameter.
4. `user` - name of database user which application should use to connect to the database. 
5. `password` - password for the database.
6. `mappings` - column names that should be mapped to the user properties. If mappings are not provided, the application will use properties names as column names.

Example of application.yaml configuration file:

```
ds-validate-mode: soft
data-sources:
  - name: db1
    url: jdbc:postgresql://192.168.56.102:5432/users
    table: users
    user: postgres
    password: sample_password
    mappings:
      id: ldap_login 
      username: ldap_login
      name: first_name
      surname: last_name
  - name: db2
    url: jdbc:postgresql://192.168.56.102:5432/users_backup
    table: users
    user: postgres
    password: sample_password
    mappings:
      id: id_number 
      username: username
      name: name
      surname: surname
```


